import { animate, animateChild, group, query, stagger, state, style, transition, trigger } from "@angular/animations";

export const routerAnimation = trigger('routeAnimations', [
  transition('* <=> *', [

    style({ position: 'relative' }),
    query(':enter, :leave', [
      style({
        position: 'absolute',
        width: '100%'
      })
    ], { optional: true }),
    query(':enter', [style({ opacity: 0 })], { optional: true }),
    query(':leave', animateChild(), { optional: true }),
    group([
      query(':leave', [animate('1s ease', style({ opacity: 0 }))], { optional: true }),
      query(':enter', [animate('1s ease-out', style({ opacity: 1 }))], { optional: true })
    ]),
    query(':enter', animateChild(), { optional: true })
  ])
])