import { Component, OnInit } from '@angular/core';
import { faAt, faPhone } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  faPhone = faPhone;
  faAt=faAt

  constructor() { }

  ngOnInit(): void {
  }

}
