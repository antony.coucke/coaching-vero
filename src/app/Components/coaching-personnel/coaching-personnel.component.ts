import { Component, OnInit } from '@angular/core';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-coaching-personnel',
  templateUrl: './coaching-personnel.component.html',
  styleUrls: ['./coaching-personnel.component.scss']
})
export class CoachingPersonnelComponent implements OnInit {
  faArrowRight = faArrowRight;

  constructor() { }

  ngOnInit(): void {
  }

}
