import { Component, OnInit } from '@angular/core';
import { faFacebookSquare,faInstagram,faLinkedin } from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  faFacebookSquare = faFacebookSquare;
  faInstagram = faInstagram;
  
  constructor() { }

  ngOnInit(): void {
  }

}
