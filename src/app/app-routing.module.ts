import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BienEtreComponent } from './Components/bien-etre/bien-etre.component';
import { CoachingPersonnelComponent } from './Components/coaching-personnel/coaching-personnel.component';
import { ContactComponent } from './Components/contact/contact.component';
import { HomeComponent } from './Components/home/home.component';
import { PresentationComponent } from './Components/presentation/presentation.component';

const routes: Routes = [  
  { path: '', component: HomeComponent },
  { path: 'presentation', component: PresentationComponent },
  { path: 'coaching-individuel', component: CoachingPersonnelComponent },
  { path: 'bien-etre', component: BienEtreComponent },
  { path: 'contact', component: ContactComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
