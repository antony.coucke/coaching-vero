import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MailService {

  constructor(private http: HttpClient) { }

  sendMail(data) {
    /*

    
      "From": {
        data.email
      },
      "TextPart": data.object,
      message: data.message,
      to: 'coucke.antony@gmail.com'

      */

    this.http.post('https://api.mailjet.com/v3.1/send ', {
      'Messages': {
        'From': {
          "Email": "coucke.antony@gmail.com",
          "Name": "Anthony"
        },
        "To": [
          {
            "Email": "coucke.antony@gmail.com",
            "Name": "Anthony"
          }
        ],
        "Subject": "My first Mailjet email",
        "TextPart": "Greetings from Mailjet.",
        "HTMLPart": "<h3>Dear passenger 1, welcome to <a href=https://www.mailjet.com/>Mailjet</a>!</h3><br />May the delivery force be with you!",
        "CustomID": "AppGettingStartedTest"
      }
    }, {
      headers: {
        'Authorization': 'Basic ' + btoa('209f84f8269dc48bc4396474e7cc98c2:736549b6c9d19d560dce79501d20b556'),
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin' : '*'
      }
    }).subscribe((res) => {
      console.log(res)
    });
  }
}